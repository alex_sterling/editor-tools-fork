﻿using System;
using UnityEngine;
using System.Text.RegularExpressions;

[KSPAddon(KSPAddon.Startup.EditorAny, false)]
public class EdTools : MonoBehaviour
{
    public static GameObject GameObjectInstance;
    private bool sceneSkip;
    private Game game;
    private bool hotkeysEnabled;
    private static float[] angle = { 90, 45, 30, 15, 5, 1, 0 };
    private String msg;
    private float msgTime;

    public void Awake()
    {
        DontDestroyOnLoad(this);
        OnLevelWasLoaded();
        
        // Parse command line
//        string[] args = System.Environment.GetCommandLineArgs();
//
//        foreach (string arg in args)
//        {
//            // Check for ship autoload
//            Match match = Regex.Match(arg, @"[\\/](VAB|SPH)[\\/]([a-z0-9\-]+)\.craft$", RegexOptions.IgnoreCase);
//            if (match.Success)
//            {
//                EditorDriver.filePathToLoad = arg;
//                EditorDriver.StartupBehaviour = EditorDriver.StartupBehaviours.LOAD_FROM_FILE;
//
//                if (match.Groups[1].Value == "SPH")
//                    PartLoader.goToSceneAfterLoad = GameScenes.SPH;
//                else
//                    PartLoader.goToSceneAfterLoad = GameScenes.EDITOR;
//                return;
//            }
//
//            // Check for destination
//            match = Regex.Match(arg.ToLower(), @"^/(vab|sph|flight|tracking)$");
//
//            if (!match.Success) continue;
//
//            sceneSkip = true;
//            game = GamePersistence.LoadGame("persistent", HighLogic.SaveFolder, true, true);
//
//            switch (match.Groups[1].Value)
//            {
//                case "vab": PartLoader.goToSceneAfterLoad = GameScenes.EDITOR; return;
//                case "sph": PartLoader.goToSceneAfterLoad = GameScenes.SPH; return;
//                case "flight": PartLoader.goToSceneAfterLoad = GameScenes.FLIGHT; return;
//                case "tracking": PartLoader.goToSceneAfterLoad = GameScenes.TRACKSTATION; return;
//            }
//        }
    }

    public void OnLevelWasLoaded()
    {
        EditorLogic editor = EditorLogic.fetch;
        if (editor)
        {
            hotkeysEnabled = true;
            editor.maxHeight = 2000;
            GameSettings.VAB_USE_ANGLE_SNAP = true;

            // Increase camera limits & VAB/SPH size
            if (HighLogic.LoadedScene == GameScenes.EDITOR)
            {
                VABCamera VABcam = Camera.mainCamera.GetComponent<VABCamera>();
                VABcam.maxHeight = 2000;
                VABcam.maxDistance = 2000;

                GameObject interior = GameObject.Find("interior_vehicleassembly");
                interior.transform.localScale = new Vector3(2.2f, 1.8f, 1.8f);
                interior.transform.position = new Vector3(59f, 51.5f, 12);
            }
            else
            {
                SPHCamera SPHcam = Camera.mainCamera.GetComponent<SPHCamera>();
                SPHcam.maxHeight = 2000;
                SPHcam.maxDistance = 2000;
                SPHcam.maxDisplaceX = 2000;
                SPHcam.maxDisplaceZ = 2000;

                GameObject interior = GameObject.Find("xport_sph3");
                interior.transform.localScale = new Vector3(12, 6, 12);
                interior.transform.position = new Vector3(-24.9f, -0.3f, 22.8f);
            }

            editor.symmetrySprite.Hide(true);
            editor.mirrorSprite.Hide(true);

            // Disable shortcut keys when ship name textarea has focus
            editor.shipNameField.commitOnLostFocus = true;
            editor.shipNameField.AddCommitDelegate((IKeyFocusable _) => { hotkeysEnabled = true; });
            editor.shipNameField.AddFocusDelegate((UITextField _) => { hotkeysEnabled = false; });
        }

        else if (HighLogic.LoadedScene == GameScenes.FLIGHT && sceneSkip)
        {
            Game game = GamePersistence.LoadGame("persistent", HighLogic.SaveFolder, true, true);
            game.startScene = GameScenes.FLIGHT;
            game.Start();
        }
        sceneSkip = false;
    }

    public void Update()
    {
        EditorLogic editor = EditorLogic.fetch;
        if (!editor || !hotkeysEnabled) return;

        bool alt = Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt) || Input.GetKey(KeyCode.AltGr);
        bool shift = Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);

        // X : Set symmetry mode
        if (Input.GetKeyDown(KeyCode.X) && editor.editorType == EditorLogic.EditorMode.VAB)
        {
            // Reset symmetry level
            if (alt || (editor.symmetryMode > 97 && !shift))
                editor.symmetryMode = 0;

            // Cycle symmetry mode past 8x boundry
            else if (editor.symmetryMode == 7 && !shift)
                editor.symmetryMode = 8;

            // Only use even numbers of high level symmetry
            if (editor.symmetryMode > 11) // moo?
                editor.symmetryMode = editor.symmetryMode + (shift ? -1 : 1);
        }

        // C : Set angle snap mode
        if (Input.GetKeyDown(KeyCode.C))
        {
            GameSettings.VAB_USE_ANGLE_SNAP = false;

            int i = Array.IndexOf(angle, editor.srfAttachAngleSnap);

            if (shift)
                editor.srfAttachAngleSnap = angle[i == angle.Length - 1 ? 0 : i + 1];
            else
                editor.srfAttachAngleSnap = angle[i == 0 ? angle.Length - 1 : i - 1];

            if (editor.srfAttachAngleSnap == 0)
            {
                GameSettings.VAB_USE_ANGLE_SNAP = true;
                editor.angleSnapSprite.PlayAnim(0);
            }
        }

        // Tab : Switch between symmetry & mirror modes
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (editor.editorType == EditorLogic.EditorMode.SPH)
            {
                editor.editorType = EditorLogic.EditorMode.VAB;
                editor.launchSiteName = "LaunchPad";
                PrintMessage("Symmetry mode enabled - Taking off from: Launchpad", 3);
            }
            else
            {
                editor.editorType = EditorLogic.EditorMode.SPH;
                editor.launchSiteName = "Runway";
                PrintMessage("Mirror mode enabled - Taking off from: Runway", 3);
            }

            editor.symmetryMode = 0;
        }

        // V : Toggle vertical align
        if (Input.GetKeyDown(KeyCode.V))
            PrintMessage("Vertical snap: " + ((GameSettings.VAB_ANGLE_SNAP_INCLUDE_VERTICAL ^= true) ? "En" : "Dis") + "abled", 3);

        // ALT + R: Toggle radial attachment
        if (alt && EditorLogic.SelectedPart && Input.GetKeyDown(KeyCode.R))
            PrintMessage("Radial attachment " + ((EditorLogic.SelectedPart.attachRules.srfAttach ^= true) ? "en" : "dis") + "abled for selected part:\n" + EditorLogic.SelectedPart.partInfo.title , 3);

        // ALT + Z: Toggle part clipping
        if (alt && Input.GetKeyDown(KeyCode.Z))
            PrintMessage("Part clipping: " + ((CheatOptions.AllowPartClipping ^= true) ? "En" : "Dis") + "abled", 3);

        // CTRL: Temporarily toggle part clipping
        CheatOptions.AllowPartClipping ^= Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyUp(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.RightControl) || Input.GetKeyUp(KeyCode.RightControl);
    }
        
    public void PrintMessage(string message = "", float duration = 1)
    {
        msg = message;
        msgTime = Time.time + duration;
    }

    private void OnGUI()
    {
        EditorLogic editor = EditorLogic.fetch;
        if (!editor) return;

        GUI.skin = HighLogic.Skin;
        GUIStyle style = new GUIStyle("Label");
        style.alignment = TextAnchor.MiddleCenter;
        style.fontSize = 20;

        // Show OSD message if applicable
        if (msgTime > Time.time)
        {
            style.normal.textColor = Color.black;
            GUI.Label(new Rect(2, 2 + (Screen.height / 9), Screen.width, 50), msg, style);
            style.normal.textColor = Color.yellow;
            GUI.Label(new Rect(0, Screen.height / 9, Screen.width, 50), msg, style);
        }

        style.fontSize = 22;

        // Show Symmetry level
        string sym = (editor.symmetryMode + 1) + "x";
        if (editor.editorType == EditorLogic.EditorMode.SPH)
            sym = (editor.symmetryMode == 0) ? "M" : "MM";

        style.normal.textColor = Color.black;
        GUI.Label(new Rect(72, Screen.height - 102, 50, 50), sym, style);
        style.normal.textColor = XKCDColors.DarkYellow;
        GUI.Label(new Rect(70, Screen.height - 104, 50, 50), sym, style);

        // Show angle snap amount
        editor.angleSnapSprite.Hide(GameSettings.VAB_USE_ANGLE_SNAP);
        if (GameSettings.VAB_USE_ANGLE_SNAP)
        {
            style.normal.textColor = Color.black;
            GUI.Label(new Rect(139, Screen.height - 102, 50, 50), editor.srfAttachAngleSnap + "\u00B0", style);
            style.normal.textColor = XKCDColors.DarkYellow;
            GUI.Label(new Rect(137, Screen.height - 104, 50, 50), editor.srfAttachAngleSnap + "\u00B0", style);
        }

    }
}

//public class EdToolsInit : KSP.Testing.UnitTest
//{
//    public EdToolsInit()
//    {
//        UnityEngine.Object.DontDestroyOnLoad(new GameObject("EdTools", typeof(EdTools)));
//    }
//}

